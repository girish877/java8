package org.giri;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class _8SupplierFunctionalInterface {
	
	public static void main(String[] args) {
		
		Supplier<List> condition1 = () -> {
			List list = new ArrayList();
			list.add(1);
			list.add(2);
			return list;
		};
		
		System.out.println(condition1.get());
		
	}

}
