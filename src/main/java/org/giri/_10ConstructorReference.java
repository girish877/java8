package org.giri;

class Test {
	
	public Test(){
		System.out.println("im Test constructor...");
	}
	
}


@FunctionalInterface
interface Example6{
	public Test getTest();
}

public class _10ConstructorReference {
	
	public static void main(String[] args) {
		
		Example6 example6 = () -> new Test();
		
		example6.getTest();
		
		Example6 constructorReference = Test::new;
		
		constructorReference.getTest();
		
		
	}

}
