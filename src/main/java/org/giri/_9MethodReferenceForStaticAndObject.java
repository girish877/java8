package org.giri;

@FunctionalInterface
interface Example4{
	
	public void printTen();
}

@FunctionalInterface
interface Example5{
	public int getLength(String inputString);
}

public class _9MethodReferenceForStaticAndObject {
	
	public static void displayTen(){
		for(int i =0; i< 10; i++){
			System.out.println(i);
		}
	}
	
	public int printStringLength(String input){
		System.out.println(input.length());
		return input.length();
	}
	
	public static void main(String[] args) {
		
		Example4 staticMethodReference = _9MethodReferenceForStaticAndObject::displayTen;
		
		staticMethodReference.printTen();
		
		
		_9MethodReferenceForStaticAndObject mr = new _9MethodReferenceForStaticAndObject();
		
		Example5 objectReference = mr::printStringLength;
		
		System.out.println(objectReference.getLength("Gireesh Kalatippi"));
		
	}

}
