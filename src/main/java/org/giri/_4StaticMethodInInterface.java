package org.giri;

interface Example8{
	
	default void method1(){
		System.out.println("im mehtod1:Example8");
	}
	
	public static void method2(){
		System.out.println("im method2:Example8");
	}
}

public class _4StaticMethodInInterface implements Example8 {
	
	public static void method2(){
		System.out.println("im method2:_5StaticMethodInInterface but im not overrided mwthod as im static method :)");
	}
	
	public static void main(String[] args) {
		
		Example8.method2();
		
		_4StaticMethodInInterface test = new _4StaticMethodInInterface();
		test.method1();
		_4StaticMethodInInterface.method2();
	}

}
