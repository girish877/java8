package org.giri;

@FunctionalInterface
interface Example1{
	public void dispaly(); 
}

@FunctionalInterface
interface Example2{
	public void add(int a, int b);
}

@FunctionalInterface
interface Example3{
	public int getLength(String name);
}

public class _1LymdaExample {
	
	public static void main(String[] args) {
		
		Example1 eg1 = () -> {System.out.println("Hello World!..");};
		
		Example1 eg11 = () -> System.out.println("Hellow world without {}");
		
		eg1.dispaly();
		
		eg11.dispaly();
		
		
		Example2 eg2 = (int a, int b) -> {System.out.println(a+b);};
		
		Example2 eg22 = (int a, int b) -> System.out.println(a+b);
		
		Example2 eg222 = (a,b) -> System.out.println(a+b);
		
		eg2.add(10, 10);
		
		eg22.add(20, 20);
		
		eg222.add(30, 30);
		
		Example3 eg3 = (String name) -> {return name.length();};
		
		Example3 eg33 = (String name) -> name.length();
		
		Example3 eg333 = (name) -> name.length();
		
		Example3 eg3333 = name -> name.length();
		
		System.out.println(eg3.getLength("Gireesh"));
		
		System.out.println(eg33.getLength("Kalatippi"));
		
		System.out.println(eg333.getLength("Girish"));
		
		System.out.println(eg3333.getLength("Kaltippi"));
		
		
		
		
	}

}
