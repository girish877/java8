package org.giri;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class _11Streams {
	
	public static void main(String[] args) {
		
		ArrayList<Integer> list = new ArrayList<>();
		list.add(12);
		list.add(11);
		list.add(22);
		list.add(21);
		list.add(32);
		list.add(31);
		list.add(42);
		list.add(41);
		list.add(52);
		list.add(51);
		
		System.out.println("Main List : "+list);
		
		
		List<Integer> list1 = list.stream().filter(i -> i%2 == 0).collect(Collectors.toList());
		
		System.out.println("Filtered List : "+list1);
		
		List<Integer> list2 = list.stream().map(i -> i*2).collect(Collectors.toList());
		
		System.out.println("Mapped List : "+list2);
		
		List<Integer> list3 = list.stream().sorted().collect(Collectors.toList());
		
		System.out.println("Default Sorted List : "+list3);
		
		List<Integer> list4 = list.stream().sorted((i1,i2) -> -i1.compareTo(i2)).collect(Collectors.toList());
		
		System.out.println("Custom Sorted List : "+list4);
		
		Integer max =list.stream().max((i1,i2) -> i1.compareTo(i2)).get();
		
		System.out.println("Max no. in list : "+max);
		
		Integer min = list.stream().min((i1,i2) -> i1.compareTo(i2)).get();
		
		System.out.println("Min no. in list : "+min);
		
		System.out.println("By using forEach method - normal way");
		list.stream().forEach(i -> System.out.println(i));
		
		System.out.println("By using forEach method - method reference way");
		list.stream().forEach(System.out::println);
		
		System.out.println("Converting List to Array's ");
		Integer[] array = list.stream().toArray(Integer[]::new);
		
		for(Integer i : array){
			System.out.println(i);
		}
		
		System.out.println("Stream api's even can applied on group of items or an array's");
		
		Stream data = Stream.of(1,2,3,4,5,6,7,8,9,10);
		data.forEach(System.out::println);
		
		System.out.println("-------");
		String[] names = {"gireesh","kalatippi","bijapur","586103"};
		Stream.of(names).forEach(System.out::println);;
		
		
	}

}
