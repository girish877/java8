package org.giri;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

public class _5PrdicateExample {
	
	public static void main(String[] args) {
		
		Predicate<Integer> conditionCheck1 = input -> input > 10;
		
		System.out.println("To check given no greater than 10 : "+conditionCheck1.test(100));
		
		System.out.println("TO check givn no greater than 10 : "+conditionCheck1.test(1));
		
		Predicate<String> conditionCheck2 = input -> input.contains("giri");
		
		System.out.println("To check given string having giri : "+conditionCheck2.test("Gireesh"));
		
		System.out.println("To check given string having giri : "+conditionCheck2.test("girish"));
		
		Predicate<Collection> conditionCheck3 = input -> input.isEmpty();
		
		List list = new ArrayList<>();
		
		System.out.println("To check given collection is empty :"+conditionCheck3.test(list));
		
		list.add(1);
		
		System.out.println("To check given collection is empty :"+conditionCheck3.test(list));
		
		System.out.println("To check given no greater than 10 : "+conditionCheck1.negate().test(100));
		
		Predicate<Integer> conditionCheck4 = input -> input % 2 == 0;
		
		System.out.println("The joining of prediactes : "+ conditionCheck1.and(conditionCheck4).test(12));
		
		System.out.println("The joining of prediactes : "+ conditionCheck1.or(conditionCheck4).test(12));
		
		
	}

}
