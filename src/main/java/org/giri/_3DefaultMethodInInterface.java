package org.giri;

interface Example7{
	
	default int getStringLength(String input){
		System.out.println("im default method:getStringLengt ");
		return input.length();
	}
	
	default boolean checkForEquality(String input1, String input2){
		System.out.println("im default method:checkForEquality");
		return input1.equalsIgnoreCase(input2);
	}
	
	/*default int hashCode(){ // which throws compile time error - we cant have any methods from Object class.
		return 1000;
	}*/
	
}

public class _3DefaultMethodInInterface implements Example7{
	
	public boolean checkForEquality(String input1, String input2){
		System.out.println("Im overrided method in current class..");
		return input1.equals(input2);
	}
	
	public void doSomeWork(_3DefaultMethodInInterface test){
		
		Example7.super.checkForEquality("Gireesh", "gireesh");
		
		System.out.println(test.getStringLength("Gireesh Kalatippi"));
		System.out.println(test.checkForEquality("giri", "Giri"));
	}
	
	public static void main(String[] args) {
		
		_3DefaultMethodInInterface test = new _3DefaultMethodInInterface();
		
		test.doSomeWork(test);
		
	}

}
