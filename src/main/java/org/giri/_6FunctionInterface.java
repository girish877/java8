package org.giri;

import java.util.function.Function;

public class _6FunctionInterface {
	
	public static void main(String[] args) {
		
		Function<String, Integer> process1 = name -> name.length();
		
		System.out.println(process1.apply("girish"));
		
	}

}
