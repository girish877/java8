package org.giri;




public class _2RunnableLambdaExample {
	
	public static void main(String[] args) {
		
		Runnable runnable = () -> System.out.println("This is executing by thread name : "+Thread.currentThread().getName());
		
		Thread t1 = new Thread(runnable);
		
		t1.start();
		
		new Thread(() -> System.out.println("This is executing by thread name : "+Thread.currentThread().getName())).start();;
		
	}

}
